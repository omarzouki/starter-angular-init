import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { HomeComponent } from './home/components/home/home.component';
import { ListItemsComponent } from './items/containers/list-items/list-items.component';
import { PagenotfoundComponent } from './page-not-found/components/pagenotfound/pagenotfound.component';

const appRoutes: Routes = [
  {path: 'items', loadChildren: 'app/items/items.module#ItemsModule'},
   { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  // { path: '**', component: PagenotfoundComponent }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forRoot( appRoutes,{preloadingStrategy: PreloadAllModules},
      // { enableTracing: true } // <-- debugging purposes only
    )
  ],
  declarations: [],
  exports: [ RouterModule]
})
export class AppRoutingModule { }
