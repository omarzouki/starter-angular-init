import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../../../shared/models/item.model';
import { State } from '../../../shared/enums/state.enum';
import { Router } from '@angular/router';
import { ItemService } from '../../../core/services/items/item.service';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  @Input() item: Item;
  state = State;
  data: Observable<Item>;

  constructor( private router: Router, private itemService: ItemService) { }

  ngOnInit() {
    this.data = this.itemService.getItem(this.item.id);
    this.data.subscribe((donnee) => console.log(donnee));
  }

  goEdit() {
    this.router.navigate(['/edit', this.item]);
  }

}
