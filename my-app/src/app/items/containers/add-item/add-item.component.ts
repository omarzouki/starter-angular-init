import { Component, OnInit } from '@angular/core';
import { ItemService } from '../../../core/services/items/item.service';
import { Item } from '../../../shared/models/item.model';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss']
})
export class AddItemComponent implements OnInit {

  constructor( private itemService: ItemService) { }

  ngOnInit() {
  }

  addItem(newItem: Item): void {
    this.itemService.add(newItem);
  }
}
