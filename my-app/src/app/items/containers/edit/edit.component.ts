import { Component, OnInit } from '@angular/core';
import { ItemService } from '../../../core/services/items/item.service';
import { Item } from '../../../shared/models/item.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  item: Item;

  constructor(private itemService: ItemService, private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe((data) => {this.item = data['item'];
      console.log('depuis edit component' + this.item.name);
  });
  }

  addItem(newItem: Item): void {
    this.itemService.update(newItem);
  }
}
