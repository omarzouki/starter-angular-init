import { Component, OnInit } from '@angular/core';
import { ItemService } from '../../../core/services/items/item.service';
import { Observable } from 'rxjs/Observable';
import { Item } from '../../../shared/models/item.model';

@Component({
  selector: 'app-list-items',
  templateUrl: './list-items.component.html',
  styleUrls: ['./list-items.component.scss']
})
export class ListItemsComponent implements OnInit {
  collection$: Observable<Item[]>;

  constructor( private itemsService: ItemService) { }

  ngOnInit() {
    this.collection$ = this.itemsService.getCollection();
    // console.log(this.collection);
    // this.collection$.subscribe(data) => {console.log(data)};
  }

}
