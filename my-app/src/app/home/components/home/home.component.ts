import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  title: String;

  constructor() { }

  ngOnInit() {
    this.title = 'coucou a cette formation Angular5';
  }

}
