import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { RouterModule, Router } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
// import { ItemsModule } from './items/items.module';
import { HomeModule } from './home/home.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PageNotFoundModule } from './page-not-found/page-not-found.module';
import { SharedModule } from './shared/shared.module';

import { AppComponent } from './app.component';
// import { environment } from '../environments/environment';
import { ItemService } from './core/services/items/item.service';
import { Router } from '@angular/router';
import { environment } from '../environments/environment.prod';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { EditComponent } from './items/containers/edit/edit.component';
import { ItemDetailResolverService } from './core/services/item-detail-resolver/item-detail-resolver.service';



@NgModule({
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    NgbModule.forRoot(),
    CoreModule,
    SharedModule,
    AppRoutingModule,
    // ItemsModule,
    HomeModule,
    PageNotFoundModule,
  ],
  declarations: [
    AppComponent
  ],
  providers: [ ItemService, ItemDetailResolverService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(router: Router) {
    if (!environment.production) {
      console.log('Routes: ', JSON.stringify(router.config, undefined, 2));
    }
  }
}
