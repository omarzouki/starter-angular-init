import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { State } from '../../enums/state.enum';
import { Item } from '../../models/item.model';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  form: FormGroup;
  // state: State;
  stateLibelles = Object.values(State);

  @Output() itemOutput: EventEmitter<Item> = new EventEmitter();
  @Input() itemInput: Item;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.createForm();
  }

  createForm(): void {
  this.form = this.fb.group({
    name: [
      this.itemInput ? this.itemInput.name : '',
      Validators.compose([Validators.required, Validators.minLength(5)])
    ],
    reference: [
      this.itemInput ? this.itemInput.reference : '',
      Validators.compose([Validators.required, Validators.minLength(4)])
    ],
    state: [
      this.itemInput ? this.itemInput.state : State.ALIVRER
    ]
  });
  }

  process(): void {
    console.log(this.form.value);
    const datas = this.getItem();
    this.itemOutput.emit( datas );
    this.reset();
  }

  isError(champ: string) {
    return this.form.get(champ).dirty && this.form.get(champ).hasError('minlenght');
  }

  getItem(): Item {
    const data = this.form.value as Item;
    if (!this.itemInput) {
      return data; // in case  add commande <-- return this.form.value
    }
    const id = this.itemInput.id;
    return {id, ...data}; // in case edit commande <-- return this.form.value and get existing id for update in bdd
  }

  reset(): void {
    this.form.reset();
    this.form.get('state').setValue(State.ALIVRER);
  }

}
