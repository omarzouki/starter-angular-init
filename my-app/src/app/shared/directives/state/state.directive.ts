import { Input, Directive, HostBinding } from '@angular/core';
import { State } from '../../enums/state.enum';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';


@Directive({
  selector: '[appState]'
})
export class StateDirective implements OnInit{
  @Input('appState') appState: State;
  @HostBinding('class') elementClass: string;

  constructor() { console.log(this.appState)}

  ngOnInit(): void {
    console.log(this.appState);
    this.elementClass = this.formatClass(this.appState);
  }

  private removeAccent(state: string): string {
    console.log('YOOOO');
    return state.normalize('NFD').replace(/[\u0300-\u036f]/g, '' );
  }

  private formatClass(state: string): string {
    console.log('coucou');
    return `state-${this.removeAccent(state).toLowerCase().replace(' ' , '-')}`;
  }
}
