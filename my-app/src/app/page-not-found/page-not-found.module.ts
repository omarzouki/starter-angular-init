import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import { PagenotfoundroutingModule } from './pagenotfoundrouting.module';

@NgModule({
  imports: [
    CommonModule, PagenotfoundroutingModule
  ],
  declarations: [PagenotfoundComponent],
  exports: [PagenotfoundComponent]
})
export class PageNotFoundModule { }
