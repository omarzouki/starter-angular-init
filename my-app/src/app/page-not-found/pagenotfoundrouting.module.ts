import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  { path: '**', component: PagenotfoundComponent },
 ];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild( appRoutes )
  ],
  declarations: [],
  exports: [ RouterModule]
})
export class PagenotfoundroutingModule { }
