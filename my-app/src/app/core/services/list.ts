import { Item } from '../../shared/models/item.model';

export const ITEMS: Item[] = [
  {
    id: 'jkhjkhjdfhkd',
    name: 'cindy',
    reference: '1234',
    state : 'A livrer'
  },
  {
    id: 'ajkhjkhjdfhkd',
    name: 'kenny',
    reference: '5678',
    state : 'En cours'
  },
  {
    id: 'bjkhjkhjdfhkd',
    name: 'john',
    reference: '9876',
    state : 'Livrée'
  },
]
