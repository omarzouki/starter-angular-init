import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, Router, ActivatedRouteSnapshot } from '@angular/router';
import { ItemService } from '../items/item.service';

import 'rxjs/add/operator/take';
import { Observable } from 'rxjs/Observable';
import { Item } from '../../../shared/models/item.model';

@Injectable()
export class ItemDetailResolverService implements Resolve<Item> {

  constructor(private itemService: ItemService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const id = route.paramMap.get('id');

    return this.itemService.getItem(id).take(1).map(data => {
      if (data) {
        // console.log(`item form resolver : ${this.item.name}`);
        return data;
      } else { // if not found
        this.router.navigate(['/items']);
        return null;
      }
    });
  }
}
